/*
 * @Author: zhou lei
 * @Date: 2024-03-20 10:16:33
 * @LastEditTime: 2024-03-20 15:07:19
 * @LastEditors: zhoulei 
 * @Description: Description
 * @FilePath: /vue3_ts_three/src/stores/index.ts
 * 
 */
import { createPinia } from 'pinia'
const pinia = createPinia()
export default pinia
