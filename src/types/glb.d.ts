/*
 * @Author: zhou lei
 * @Date: 2024-01-31 14:16:35
 * @LastEditTime: 2024-04-01 17:07:16
 * @LastEditors: zhoulei && 910592680@qq.com
 * @Description: Description
 * @FilePath: /vue3_ts_three/src/types/glb.d.ts
 *
 */
// declare module '*.glb' {
//   const src: string
//   export default src
// }
declare module '*.glb'
declare module '*.hdr'
declare module '*.vue'
declare module 'vue-draggable-resizable'
